const express = require("express");
const http = require("http");
const socketIO = require("socket.io");

var connection_string = "localhost:27017/local";
// Connection string of MongoDb database hosted on Mlab or locally
// Collection name should be "FoodItems", only one collection as of now.
// Document format should be as mentioned below, at least one such document:
// {
//     "_id": {
//         "$oid": "5c0a1bdfe7179a6ca0844567"
//     },
//     "name": "Veg Roll",
//     "predQty": 100,
//     "prodQty": 295,
//     "ordQty": 1
// }


// main:
// id,ordSt,melSt,mdfSt,apprDate,assyDate,inv,client,ordBy,po,tag,street,ordType,color,drType,cabCt,drCt,note,footage,dwg,wall1,hw,org,drMat,ordAC,dateF,dateOut,tfgDate,edgeDate,sandDate,melDate,mdfDate,wall2
// assy:
// assyDate,ordSt,id,ordType,color,drType,client,tag,cabCt,assyNotes
// doors:
// [specific]St,id,color,drType,drCt,[specific]date,[previous]status
// 

const db = require("monk")(connection_string);

const collection_foodItems = db.get("afo");

// our localhost port
const port = process.env.PORT || 3001;

const app = express();

// our server instance
const server = http.createServer(app);

// This creates our socket using the instance of the server
const io = socketIO(server);

io.on("connection", socket => {
  console.log("New client connected: " + socket.id);
  //console.log(socket);
  //collection_foodItems.find({}).then(x=>{console.log(x)});

  // Returning the initial data of food menu from FoodItems collection
  socket.on("initial_data", ({rowFilter,colFilter,tableName}) => {
    collection_foodItems.find(rowFilter,colFilter).then(docs => {
      io.sockets.emit("get_data", [docs,tableName]);
      console.log("emitting updated data")
    });
  });

// assy:
// assyDate,ordSt,id,ordType,color,drType,client,tag,cabCt,assyNotes
  socket.on("assembly",()=>{
    console.log(workDay(-1));
    let rowFilter={
      $or:[
        {assyDate:workDay(-1)},
        {assyDate:workDay(0)},
        {assyDate:workDay(1)}
      ]
    };
    let colFilter = ['_id','assyDate','ordSt','id','ordType','color','drType','client','tag','cabCt','assyNotes'];
    collection_foodItems.find(rowFilter,colFilter).then(docs => {console.log(docs);io.sockets.emit("get_data", docs);})
  });

  // doors:
  // [specific]St,id,color,drType,drCt,[specific]date,[previous]status

  socket.on("subShop",(shop)=>{
    let rowFilter={
      $or:[
        {assyDate:workDay(-1)},
        {assyDate:workDay(0)},
        {assyDate:workDay(1)}
      ]
    };
    let colFilter = ['_id','assyDate','ordSt','id','ordType','color','drType','client','tag','cabCt','assyNotes'];
    collection_foodItems.find(rowFilter,colFilter).then(docs => {console.log(docs);io.sockets.emit("get_data", docs);})
  });

  socket.on("inProduction", () => {
    collection_foodItems.find({
        $or:[{ordSt:"tasked"},{ordSt:"production"},{ordSt:"finished"}]
      }).then(docs => {
      io.sockets.emit("get_data", docs);
    });
  });

  // Placing the order, gets called from /src/main/PlaceOrder.js of Frontend
  socket.on("putOrder", order => {
    collection_foodItems
      .update({ _id: order._id }, { $inc: { ordQty: order.order } })
      .then(updatedDoc => {
        // Emitting event to update the Kitchen opened across the devices with the realtime order values
        io.sockets.emit("change_data");
      });
  });

  // Order completion, gets called from /src/main/Kitchen.js
  socket.on("mark_done", ord => {
    collection_foodItems
      .update({ _id: ord._id }, { $set: { entryDate:ord.entryDate,client:ord.client,tag:ord.tag,ordType:ord.ordType } })
      .then(updatedDoc => {
        //Updating the different Kitchen area with the current Status.
        io.sockets.emit("change_data");
      });
  });

  // Functionality to change the predicted quantity value, called from /src/main/UpdatePredicted.js
  socket.on("ChangePred", predicted_data => {
    collection_foodItems
      .update(
        { _id: predicted_data._id },
        { $set: { id: 8003}}//predQty: predicted_data.predQty } }
      )
      .then(updatedDoc => {
        // Socket event to update the Predicted quantity across the Kitchen
        io.sockets.emit("change_data");
      });
  });


  socket.on("canc", () => {
    console.log("canc");
    collection_foodItems.find(
      {$or:[{assyDate:"07/11/2019"},{assyDate:"07/10/2019"},{assyDate:"07/09/2019"}]},
      (err,docs)=>{console.log(docs)}
    )//.then(x=>{console.log(x.length)});
  });

  // Functionality to change the predicted quantity value, called from /src/main/UpdatePredicted.js
  socket.on("editOrd", ord => {
    console.log("editing "+ord.id)
    //console.log(ord)
    //collection_foodItems.find(statusFilter,(err,docs)=>{console.log(docs.length)})//.then(x=>{console.log(x.length)});
    collection_foodItems
      .update(
        { _id: ord._original._id },
        { $set: { id:ord.id,entryDate:ord.entryDate,client:ord.client,tag:ord.tag,ordType:ord.ordType}}//predQty: predicted_data.predQty } }
      )
      .then(updatedDoc => {
        // Socket event to update the Predicted quantity across the Kitchen
        io.sockets.emit("change_data");
      });
  });

  socket.on("editVal",data=>{
    console.log('editing from "'+data.oldValue+'" to "'+data.newValue+'" in the '+data.name+" field of "+data.id);
    collection_foodItems.update({_id:data._id},{$set:{[data.name]:data.newValue}})
      .then(io.sockets.emit("change_data"));
  });

  socket.on("newOrd",data=>{
    console.log('Creating order '+data.id);
    let editedData = {...data,id:parseInt(data.id)}
    collection_foodItems.insert(editedData)
      .then(io.sockets.emit("change_data"));
  });

  // disconnect is fired when a client leaves the server
  socket.on("disconnect", () => {
    console.log("user disconnected");
  });
});
/*
// lines which generate array with all weekdays in the year 2020:

let getDateFromDayNum = (dayNum, year) => {
  var date = new Date();
  if(year){
      date.setFullYear(year);
  }
  date.setMonth(0);
  date.setDate(0);
  var timeOfFirst = date.getTime(); // this is the time in milliseconds of 1/1/YYYY
  var dayMilli = 1000 * 60 * 60 * 24;
  var dayNumMilli = dayNum * dayMilli;
  date.setTime(timeOfFirst + dayNumMilli);
  return date.toLocaleDateString("en-US");
}

let workDays = [];
for (let i = 1;i<366;i++){
  if(new Date(getDateFromDayNum(i,2020)).getDay()!==0&&new Date(getDateFromDayNum(i,2020)).getDay()!==6){workDays.push(getDateFromDayNum(i,2020))}
}
*/


/* Below mentioned steps are performed to return the Frontend build of create-react-app from build folder of backend */

app.use(express.static("build"));
app.use("/kitchen", express.static("build"));
app.use("/updatepredicted", express.static("build"));

server.listen(port, () => console.log(`Listening on port ${port}`));
