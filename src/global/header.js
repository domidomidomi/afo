import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./header.css";

// The Header creates links that can be used to navigate
// between routes.
class Header extends Component {
  /*constructor() {
    super();
    this.state = {
      endpoint: "http://localhost:3001/" // Update 3001 with port on which backend-my-app/server.js is running.
    };

    socket = socketIOClient(this.state.endpoint);
  }*/

  render() {
    return (
      <header>
        <nav>
          <ul className="NavClass">
            <li>
              <NavLink to="/plan">Plan</NavLink>
            </li>
            <li>
              <NavLink exact to="/">Orders</NavLink>
            </li>
            <li>
              <NavLink to="/prod">ProductionWIP</NavLink>
            </li>
            <li>
              <NavLink to="/assy">Assembly</NavLink>
            </li>
            <li>
              <NavLink to="/tfd">TFD</NavLink>
            </li>
            <li>
              <NavLink to="/sand">Sand</NavLink>
            </li>
            <li>
              <NavLink to="/edge">Edging</NavLink>
            </li>
            <li>
              <NavLink to="/gantt">Gantt</NavLink>
            </li>
          </ul>
        </nav>
      </header>
    );
  }
}

export { Header };
