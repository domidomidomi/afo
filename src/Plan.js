import React, { Component } from 'react';
import io from 'socket.io-client';
import './datepicker.css';
import './App.css';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-dark.css';
import $ from 'jquery';
window.jquery = window.jQuery = $;


let socket = io('http://localhost:3001/');

class Plan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columnDefs: [
        {headerName: "EntryDate", field: "entryDate", editable:true, cellEditor:'datePicker'},
        {headerName: "id", field: "id", editable:true},
        {headerName: "Order State", field: "ordSt", editable:true,cellEditor:'agSelectCellEditor',cellEditorParams:{cellHeight:40,values:['pending','processed','tasked','production','finished','out']}},
        {headerName: "Client", field: "client", editable:true}, 
        {headerName: "Tag", field: "tag", editable:true}, 
        {headerName: "Order Type", field: "ordType", editable:true}, 
        {headerName: "Color", field: "color", editable:true}, 
        {headerName: "Door Type", field: "drType", editable:true}, 
        {headerName: "Cab Count", field: "cabCt", editable:true}, 
        {headerName: "Door Count", field: "drCt", editable:true}, 
        {headerName: "Mel State", field: "melSt", editable:true},
        {headerName: "MDF State", field: "mdfSt", editable:true},  
        {headerName: "AssemblyDate", field: "assyDate", editable:true, cellEditor: 'datePicker'},
        {headerName: "TFD Date", field: "tfdDate", editable:true, cellEditor: 'datePicker'}, 
        {headerName: "Edge Date", field: "edgeDate", editable:true, cellEditor: 'datePicker'}, 
        {headerName: "Sand Date", field: "sandDate", editable:true, cellEditor: 'datePicker'}, 
        {headerName: "Mel Cut Date", field: "melDate", editable:true, cellEditor: 'datePicker'}, 
        {headerName: "MDF Cut Date", field: "mdfDate", editable:true, cellEditor: 'datePicker'} 
      ],
      gridOptions:{components: { datePicker: this.getDatePicker() }},
      rowData: []
    }
  }


  componentDidMount() {
    var state_current = this;
    socket.emit("initial_data");//("assembly");//
    socket.on("get_data", this.getData);
    socket.on("change_data", this.changeData);

    let script = document.createElement("script");
    script.src="//code.jquery.com/ui/1.11.4/jquery-ui.js"
    script.async = true;

    document.body.appendChild(script);

  }

  changeData = () => {socket.emit("initial_data")};

  componentWillUnmount() {
    socket.off("get_data");
    socket.off("change_data");
  }

  editCellStart=(params)=>{
    //console.log(params)
  }

  handleCellChange=(e)=>{
    console.log(e);
    let data = {_id:e.data._id,id:e.data.id,name:e.colDef.field,newValue:e.newValue,oldValue:e.oldValue}
    socket.emit("editVal",data);
  }

  getDatePicker=()=>{
    function Datepicker() {}
    Datepicker.prototype.init = function(params) {
      this.eInput = document.createElement("input");
      this.eInput.value = params.value;
      $(this.eInput).datepicker({ dateFormat: "m/d/yy" });
    };
    Datepicker.prototype.getGui = function() {
      return this.eInput;
    };
    Datepicker.prototype.afterGuiAttached = function() {
      this.eInput.focus();
      this.eInput.select();
    };
    Datepicker.prototype.getValue = function() {
      return this.eInput.value;
    };
    Datepicker.prototype.destroy = function() {};
    Datepicker.prototype.isPopup = function() {
      return false;
    };
    return Datepicker;
  }

  getData = (ords) => {
    this.setState({ rowData: ords },()=>{console.log(this.state.rowData)});
    return null;
  };

  render() {
    return (
      <div 
        className="ag-theme-dark"
        style={{ 
        height: '500px', 
        width: '1100px' }} 
      >
        <AgGridReact
          gridOptions={this.state.gridOptions}
          onCellEditingStarted={params=>this.editCellStart(params)}
          onCellValueChanged={e=>this.handleCellChange(e)}
          columnDefs={this.state.columnDefs}
          rowData={this.state.rowData}>
        </AgGridReact>
      </div>
    );
  }
}

export default Plan;