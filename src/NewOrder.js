import React, { useState } from 'react';
import { Collapse, Button, CardBody, Card, Form, FormGroup, Label, Input, FormText  } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const NewOrder = ({cols,submit}) => {

    const [collapse, setCollapse] = useState(false);
    let blanks = [];
    cols.forEach(x=>{blanks[x.field]=""});
    const [field, setField] = useState(blanks);

    const toggle = () => setCollapse(!collapse);

    const handleChange = (e) => {
        // console.log(e)
        e.persist();
        setField(values=>({...values,[e.target.name]:e.target.type=="date"?new Date(e.target.value).toLocaleDateString('en',{month:'2-digit',day:'2-digit',year:'numeric'}):e.target.value}));
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        field.entryDate=new Date().toLocaleDateString('en',{month:'2-digit',day:'2-digit',year:'numeric'});
        let ordStOptions = cols.filter(x=>x.field==="ordSt")[0].cellEditorParams.values;
        field.ordSt=ordStOptions.includes(field.ordSt)?field.ordSt:"entered";
        submit(field);
        console.log(field);
        document.getElementById("orderForm").reset();
        toggle();
        setField({});
    }

    const form = [];
    const tempForm = []; 
    cols.forEach((x,i)=>{tempForm.push(<div className="col" key={"colElement"+i}>
                <FormGroup key={x.field} style={{width:x.field==="note"?x.width*2:x.field==="entryDate"||x.field==="assyDate"||x.field==="apprDate"||x.field==="rdyDate"||x.field==="outDate"||x.field==="tfdDate"||x.field==="edgeDate"||x.field==="sandDate"||x.field==="sprDate"||x.field==="melDate"||x.field==="mdfDate"?x.width*1.9:x.width}}>
                    <Label for={x.field}>
                        {x.headerName}
                    </Label>
                    {x.cellEditor=="agSelectCellEditor"
                    ?
                    <div><input type="text" name={x.field} list={"list-"+x.field} onChange={handleChange} autoComplete="off" />
                    <datalist id={"list-"+x.field}>
                        {x.cellEditorParams.values.map(y=><option key={"option-"+y}>{y}</option>)}
                    </datalist>
                    </div>
                    :
                    <Input type={x.cellEditor=="datePicker"?"date":"text"} name={x.field} onChange={handleChange} autoComplete="off"/>}
                </FormGroup>
            </div>)
        })

    for (let i = 0;i<tempForm.length/5;i++){
        form.push(<div className="row" key={"rowElement"+i}>{tempForm[5*i+0]}{tempForm[5*i+1]}{tempForm[5*i+2]}{tempForm[5*i+3]}{tempForm[5*i+4]}</div>)
    }


    return (
        <div className="text-left">
        <Button outline color="secondary" size="sm" onClick={toggle} style={{ marginBottom: '1rem' }}>New Order</Button>
        <Collapse isOpen={collapse}>
            <Card>
            <CardBody>
                <Form onSubmit={e=>handleSubmit(e)} id="orderForm" className="container">
                {form}
                <Button>Submit</Button>
                </Form>
            </CardBody>
            </Card>
        </Collapse>
        </div>
    );
}

export default NewOrder;