import React, { Component } from 'react';
import Related from './Related';
import io from 'socket.io-client';
import './datepicker.css';
import './App.css';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-dark.css';
import $ from 'jquery';
window.jquery = window.jQuery = $;


let socket = io('http://localhost:3001/');
// let socket = io('http://delaware:3001/');

class Orders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columnDefs: [],
      gridOptions:{
        suppressScrollOnNewData:true,
        enableCellTextSelection:true,
        autoSizePadding:0,
        headerHeight:25,
        onGridReady:params=>{params.api.setSortModel(this.props.sort)},
        components: { datePicker: this.getDatePicker() },
        getRowHeight:(params)=>this.rowHeight(params),
        getRowStyle:params=>this.rowStyle(params),
        onColumnResized:e=>console.log(e.columns[0].actualWidth),

      },
      rowData: []
    }
    this.submitNewOrd = this.submitNewOrd.bind(this)
  }


  componentDidMount() {
    //var state_current = this;
    socket.emit("initial_data",{rowFilter:this.props.rowFilter,colFilter:this.props.colFilter,tableName:this.props.tableName});//("assembly");//
    socket.on("get_data", this.getData);
    socket.on("change_data", this.changeData);

    let script = document.createElement("script");
    script.src="//code.jquery.com/ui/1.11.4/jquery-ui.js"
    script.async = true;

    document.body.appendChild(script);

    this.setState({columnDefs:this.props.columnDefs,gridOptions:{...this.state.gridOptions,onGridReady:params=>{params.api.setSortModel(this.props.sort)}}});
    if(typeof(this.props.setSubmit)==='function'){this.props.setSubmit(this.submitNewOrd)}
    console.log(this.props)
  }

  changeData = () => {socket.emit("initial_data",{rowFilter:this.props.rowFilter,colFilter:this.props.colFilter,tableName:this.props.tableName})};

  componentWillUnmount() {
    socket.off("get_data");
    socket.off("change_data");
  }

  // editCellStart=(params)=>{
  //   //console.log(params)
  // }

  handleCellChange=(e)=>{
    if (e.newValue===e.oldValue){return}
    this.showRelated({field:"id"});
    console.log('order '+e.data.id+' '+e.colDef.headerName+' value change from "'+e.oldValue+'" to "'+e.newValue+'"');
    let data = {_id:e.data._id,id:e.data.id,name:e.colDef.field,newValue:e.newValue,oldValue:e.oldValue}
    socket.emit("editVal",data);
    return null
  }

  submitNewOrd=(data)=>{
    socket.emit("newOrd",data);
  }
  getDatePicker=()=>{
    function Datepicker() {}
    Datepicker.prototype.init = function(params) {
      this.eInput = document.createElement("input");
      this.eInput.value = params.value;
      $(this.eInput).datepicker({ dateFormat: "m/d/yy" });
    };
    Datepicker.prototype.getGui = function() {
      return this.eInput;
    };
    Datepicker.prototype.afterGuiAttached = function() {
      this.eInput.focus();
      this.eInput.select();
    };
    Datepicker.prototype.getValue = function() {
      return this.eInput.value;
    };
    Datepicker.prototype.destroy = function() {};
    Datepicker.prototype.isPopup = function() {
      return false;
    };
    return Datepicker;
  }

  ganttValComb = (dates) => {
    let notation = {outDate:"out ",assyDate:"asy ",tfdDate:"tfd ",edgeDate:"edg ",sprDate:"spr ",sandDate:"snd ",melDate:"mel ",mdfDate:"mdf "};
    let ret = {};
    // Below line reads: make a set of unique date values from the dates provided. For each 
    // unique value, go through all the dates and for any key whose value matches it, add the
    // add the respective notation to the return object. If a prior entry exists, append to it.
    new Set(Object.values(dates)).forEach(x=>{for (let key in dates){if(dates[key]===x){ret[x]?ret[x]=ret[x]+notation[key]:ret[x]=notation[key]}}})
    return ret
  }

  rowStyle = (params) => {
    return params.data.accentColor?{background:params.node.rowIndex%2?'#a6a6a6':'#d9d9d9',color:'black'}:'';
  }

  rowHeight = (params) => {
    if(this.props.tableName=='edge'&&params.data.drType=="FlatMel"){return 40};
    return 20
  }

  getData = ([ordsRaw,tableName]) => {
    let accentColor = false;
    let assyDateSortedOrds = ordsRaw.sort((a,b)=>{return (new Date(a.assyDate)!="Invalid Date"&&new Date(b.assyDate)!="Invalid Date")?new Date(b.assyDate)-new Date(a.assyDate):(new Date(a.assyDate)=="Invalid Date")?-1:1});
    let ords = assyDateSortedOrds.map((x,i)=>{
      accentColor = i>1?(x.assyDate!==assyDateSortedOrds[i-1].assyDate)?!accentColor:accentColor:false
      return {...x,accentColor:accentColor}
    })
    if (tableName === this.props.tableName && tableName !== "gantt"){
      this.setState({ rowData: ords },()=>{console.log(this.state.rowData)});
    }
    if (tableName === this.props.tableName && tableName === "gantt"){
      let ganttOrds = {};
      ganttOrds=ords.map(x=>{return {...x,...this.ganttValComb({assyDate:x.assyDate,tfdDate:x.tfdDate,edgeDate:x.edgeDate,sandDate:x.sandDate,sprDate:x.sprDate,melDate:x.melDate,mdfDate:x.mdfDate,outDate:x.outDate})}})
      this.setState({ rowData: ganttOrds },()=>{console.log(this.state.rowData)});
    }
    return null;
  };

  render() {
    return (
      <div>
        <Related related={(data)=>this.setState(data)} setToggle={()=>this.toggle}/>
        <h1>{this.props.title}</h1>
        <div 
          className="ag-theme-dark"
          style={{ 
            textAlign:'left',
            bottom: '0',
            top: '120px',
            left: '0',
            right: '0',
            height: '600px',
          }} 
        >
          <AgGridReact
            gridOptions={this.state.gridOptions}
            // onCellEditingStarted={params=>this.editCellStart(params)}
            onCellValueChanged={e=>this.handleCellChange(e)}
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData}>
          </AgGridReact>
        </div>
      </div>
    );
  }
}

export default Orders;