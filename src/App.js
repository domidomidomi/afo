import React, { Component } from 'react';
import { Header } from "./global/header";
import { Switch, Route } from "react-router-dom";
import { BrowserRouter as Router } from 'react-router-dom'; 
import "./App.css";

import Orders from "./Orders.js"
import NewOrder from "./NewOrder.js"


class App extends Component {

// array with all workdays in 2019 and 2020, needs holidays dropped.
workdays = ["1/1/2019","1/2/2019","1/3/2019","1/4/2019","1/7/2019","1/8/2019","1/9/2019","1/10/2019","1/11/2019","1/14/2019","1/15/2019","1/16/2019","1/17/2019","1/18/2019","1/21/2019","1/22/2019","1/23/2019","1/24/2019","1/25/2019","1/28/2019","1/29/2019","1/30/2019","1/31/2019","2/1/2019","2/4/2019","2/5/2019","2/6/2019","2/7/2019","2/8/2019","2/11/2019","2/12/2019","2/13/2019","2/14/2019","2/15/2019","2/18/2019","2/19/2019","2/20/2019","2/21/2019","2/22/2019","2/25/2019","2/26/2019","2/27/2019","2/28/2019","3/1/2019","3/4/2019","3/5/2019","3/6/2019","3/7/2019","3/8/2019","3/11/2019","3/12/2019","3/13/2019","3/14/2019","3/15/2019","3/18/2019","3/19/2019","3/20/2019","3/21/2019","3/22/2019","3/25/2019","3/26/2019","3/27/2019","3/28/2019","3/29/2019","4/1/2019","4/2/2019","4/3/2019","4/4/2019","4/5/2019","4/8/2019","4/9/2019","4/10/2019","4/11/2019","4/12/2019","4/15/2019","4/16/2019","4/17/2019","4/18/2019","4/19/2019","4/22/2019","4/23/2019","4/24/2019","4/25/2019","4/26/2019","4/29/2019","4/30/2019","5/1/2019","5/2/2019","5/3/2019","5/6/2019","5/7/2019","5/8/2019","5/9/2019","5/10/2019","5/13/2019","5/14/2019","5/15/2019","5/16/2019","5/17/2019","5/20/2019","5/21/2019","5/22/2019","5/23/2019","5/24/2019","5/27/2019","5/28/2019","5/29/2019","5/30/2019","5/31/2019","6/3/2019","6/4/2019","6/5/2019","6/6/2019","6/7/2019","6/10/2019","6/11/2019","6/12/2019","6/13/2019","6/14/2019","6/17/2019","6/18/2019","6/19/2019","6/20/2019","6/21/2019","6/24/2019","6/25/2019","6/26/2019","6/27/2019","6/28/2019","7/1/2019","7/2/2019","7/3/2019","7/4/2019","7/5/2019","7/8/2019","7/9/2019","7/10/2019","7/11/2019","7/12/2019","7/15/2019","7/16/2019","7/17/2019","7/18/2019","7/19/2019","7/22/2019","7/23/2019","7/24/2019","7/25/2019","7/26/2019","7/29/2019","7/30/2019","7/31/2019","8/1/2019","8/2/2019","8/5/2019","8/6/2019","8/7/2019","8/8/2019","8/9/2019","8/12/2019","8/13/2019","8/14/2019","8/15/2019","8/16/2019","8/19/2019","8/20/2019","8/21/2019","8/22/2019","8/23/2019","8/26/2019","8/27/2019","8/28/2019","8/29/2019","8/30/2019","9/2/2019","9/3/2019","9/4/2019","9/5/2019","9/6/2019","9/9/2019","9/10/2019","9/11/2019","9/12/2019","9/13/2019","9/16/2019","9/17/2019","9/18/2019","9/19/2019","9/20/2019","9/23/2019","9/24/2019","9/25/2019","9/26/2019","9/27/2019","9/30/2019","10/1/2019","10/2/2019","10/3/2019","10/4/2019","10/7/2019","10/8/2019","10/9/2019","10/10/2019","10/11/2019","10/15/2019","10/16/2019","10/17/2019","10/18/2019","10/21/2019","10/22/2019","10/23/2019","10/24/2019","10/25/2019","10/28/2019","10/29/2019","10/30/2019","10/31/2019","11/1/2019","11/4/2019","11/5/2019","11/6/2019","11/7/2019","11/8/2019","11/11/2019","11/12/2019","11/13/2019","11/14/2019","11/15/2019","11/18/2019","11/19/2019","11/20/2019","11/21/2019","11/22/2019","11/27/2019","11/28/2019","11/29/2019","12/2/2019","12/3/2019","12/4/2019","12/5/2019","12/6/2019","12/9/2019","12/10/2019","12/11/2019","12/12/2019","12/13/2019","12/16/2019","12/17/2019","12/18/2019","12/19/2019","12/20/2019","12/23/2019","12/24/2019","12/25/2019","12/26/2019","12/27/2019","12/30/2019","12/31/2019","1/1/2020","1/2/2020","1/3/2020","1/6/2020","1/7/2020","1/8/2020","1/9/2020","1/10/2020","1/13/2020","1/14/2020","1/15/2020","1/16/2020","1/17/2020","1/20/2020","1/21/2020","1/22/2020","1/23/2020","1/24/2020","1/27/2020","1/28/2020","1/29/2020","1/30/2020","1/31/2020","2/3/2020","2/4/2020","2/5/2020","2/6/2020","2/7/2020","2/10/2020","2/11/2020","2/12/2020","2/13/2020","2/14/2020","2/17/2020","2/18/2020","2/19/2020","2/20/2020","2/21/2020","2/24/2020","2/25/2020","2/26/2020","2/27/2020","2/28/2020","3/2/2020","3/3/2020","3/4/2020","3/5/2020","3/6/2020","3/9/2020","3/10/2020","3/11/2020","3/12/2020","3/13/2020","3/16/2020","3/17/2020","3/18/2020","3/19/2020","3/20/2020","3/23/2020","3/24/2020","3/25/2020","3/26/2020","3/27/2020","3/30/2020","3/31/2020","4/1/2020","4/2/2020","4/3/2020","4/6/2020","4/7/2020","4/8/2020","4/9/2020","4/10/2020","4/13/2020","4/14/2020","4/15/2020","4/16/2020","4/17/2020","4/20/2020","4/21/2020","4/22/2020","4/23/2020","4/24/2020","4/27/2020","4/28/2020","4/29/2020","4/30/2020","5/1/2020","5/4/2020","5/5/2020","5/6/2020","5/7/2020","5/8/2020","5/11/2020","5/12/2020","5/13/2020","5/14/2020","5/15/2020","5/18/2020","5/19/2020","5/20/2020","5/21/2020","5/22/2020","5/25/2020","5/26/2020","5/27/2020","5/28/2020","5/29/2020","6/1/2020","6/2/2020","6/3/2020","6/4/2020","6/5/2020","6/8/2020","6/9/2020","6/10/2020","6/11/2020","6/12/2020","6/15/2020","6/16/2020","6/17/2020","6/18/2020","6/19/2020","6/22/2020","6/23/2020","6/24/2020","6/25/2020","6/26/2020","6/29/2020","6/30/2020","7/1/2020","7/2/2020","7/3/2020","7/6/2020","7/7/2020","7/8/2020","7/9/2020","7/10/2020","7/13/2020","7/14/2020","7/15/2020","7/16/2020","7/17/2020","7/20/2020","7/21/2020","7/22/2020","7/23/2020","7/24/2020","7/27/2020","7/28/2020","7/29/2020","7/30/2020","7/31/2020","8/3/2020","8/4/2020","8/5/2020","8/6/2020","8/7/2020","8/10/2020","8/11/2020","8/12/2020","8/13/2020","8/14/2020","8/17/2020","8/18/2020","8/19/2020","8/20/2020","8/21/2020","8/24/2020","8/25/2020","8/26/2020","8/27/2020","8/28/2020","8/31/2020","9/1/2020","9/2/2020","9/3/2020","9/4/2020","9/7/2020","9/8/2020","9/9/2020","9/10/2020","9/11/2020","9/14/2020","9/15/2020","9/16/2020","9/17/2020","9/18/2020","9/21/2020","9/22/2020","9/23/2020","9/24/2020","9/25/2020","9/28/2020","9/29/2020","9/30/2020","10/1/2020","10/2/2020","10/5/2020","10/6/2020","10/7/2020","10/8/2020","10/9/2020","10/12/2020","10/13/2020","10/14/2020","10/15/2020","10/16/2020","10/19/2020","10/20/2020","10/21/2020","10/22/2020","10/23/2020","10/26/2020","10/27/2020","10/28/2020","10/29/2020","10/30/2020","11/2/2020","11/3/2020","11/4/2020","11/5/2020","11/6/2020","11/9/2020","11/10/2020","11/11/2020","11/12/2020","11/13/2020","11/16/2020","11/17/2020","11/18/2020","11/19/2020","11/20/2020","11/23/2020","11/24/2020","11/25/2020","11/26/2020","11/27/2020","11/30/2020","12/1/2020","12/2/2020","12/3/2020","12/4/2020","12/7/2020","12/8/2020","12/9/2020","12/10/2020","12/11/2020","12/14/2020","12/15/2020","12/16/2020","12/17/2020","12/18/2020","12/21/2020","12/22/2020","12/23/2020","12/24/2020","12/25/2020","12/28/2020","12/29/2020","12/30/2020","12/31/2020"]

  workDay = (offset) => {
  let dblDgt=(num)=>{return num>=10?num:"0"+num};
  let date = new Date(); // add "7/9/2019" for testing
  // if today is Sun or Sat, make today be Mon
  if(date.getDay()===0){date.setDate(date.getDate()+1)};
  if(date.getDay()===6){date.setDate(date.getDate()+2)};
  if(offset?true:false){
    let index = this.workdays.indexOf(new Date(date).toLocaleDateString("en-US"));
    date = new Date(this.workdays[index+offset])
  }
  return [dblDgt(date.getMonth(2)+1),dblDgt(date.getDate()),date.getFullYear()].join('/');
}
  
formatDateMoDay=(data)=>{
  // data arrives as object with key 'value', which contains the date, formatted as 'm/dd/yyyy'
  let raw = data.value?data.value.split('/'):"";
  let mo={0:'',1:'Jan',2:'Feb',3:'Mar',4:'Apr',5:'May',6:'Jun',7:'Jul',8:'Aug',9:'Sep',10:'Oct',11:'Nov',12:'Jan'};//,01:'Jan',02:'Feb',03:'Mar',04:'Apr',05:'May',06:'Jun',07:'Jul',08:'Aug',09:'Sep'};
  let weekday = ["Sun","Mon","Tue","Wed","Thu","Fir","Sat"];
  return mo[parseInt(raw[0])]+' '+raw[1]+' '+weekday[new Date(data.value).getDay()];
}

  allColumns=[
    {headerName: "id", field: "id", filter: true , editable:true, sortable:true,resizable:true,pinned:"left",width:80},
    {headerName: "EntryD", field: "entryDate", filter: 'agDateColumnFilter', editable:true, sortable:true, resizable:true,width:110, cellEditor:'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}}, 
    {headerName: "Ord St", field: "ordSt", filter: true , editable:true, sortable:true, resizable:true,width:90,cellEditor:'agSelectCellEditor',cellEditorParams:{cellHeight:40,values:['entered','processed','approved','tasked','production','assyFwaiting','ready','out','canceled']}},
    {headerName: "#", field: "imp", filter: true , editable:true, sortable:true,resizable:true,width:32,comparator:(a,b)=>{return parseInt(a)-parseInt(b)}},
    {headerName: "Client", field: "client",  filter: true , editable:true, sortable:true, resizable:true,width:110,cellEditor:'agSelectCellEditor',cellEditorParams:{cellHeight:40,values:['c7 - APT','d0 - RFPorter','d1 - EWM','e5 - RRS','8h - Vision','w4 - Buffalo','b7 - HnS','b8 - Wdale','d6 - WReno','f5 - State','e4 - Angelo','b5 - Tadek','d9 - toni RSC','d9 - toni MPC','e0 - Reza, DR','0g - Xwood','r6 - TPD','e8 - Costa','c3 - ECO','s6 - JnP','h5 - Medallion','p8 - NG','d5 - JPS','11 - private','00 - other','e3 - QPT','af - AlphaFine','cc - Crane']}},
    {headerName: "Tag", field: "tag", filter: true , editable:true, sortable:true, resizable:true,width:115}, 
    {headerName: "Street", field: "street", filter: true , editable:true, sortable:true, resizable:true,width:120}, 
    {headerName: "Type", field: "ordType", filter: true , editable:true, sortable:true, resizable:true,width:80,cellEditor:'agSelectCellEditor',cellEditorParams:{cellHeight:40,values:['D','K','BoD','UC','LC','V','VBC L','VBC R','Vspec','IDU','IDU-P','KP','Misc','CT','CTsupp','pantry','p15 3/4','?','HdWr','panel','cabs','serv','custom']}},
    {headerName: "AssyD", field: "assyDate", filter: 'agDateColumnFilter' , editable:true, sortable:true, resizable:true,width:110, cellEditor: 'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''},comparator:(a,b)=>{return (new Date(a)!="Invalid Date"&&new Date(b)!="Invalid Date")?new Date(b)-new Date(a):(new Date(a)=="Invalid Date")?-1:1}}, 
    {headerName: "CabCt", field: "cabCt", filter: true , editable:true, sortable:true, resizable:true,width:80},
    {headerName: "DrCt", field: "drCt", filter: true , editable:true, sortable:true, resizable:true,width:75},
    {headerName: "Hw", field: "hw", filter: true , editable:true, sortable:true, resizable:true,width:80},
    {headerName: "Mel St", field: "melSt", filter: true , editable:true, sortable:true, resizable:true,width:100,cellEditor:'agSelectCellEditor',cellEditorParams:{cellHeight:40,values:['none','pending','nest-ready','nested','cut S','cut F','edge S','edge F','assy S','assyF']}},
    {headerName: "MDF St", field: "mdfSt", filter: true , editable:true, sortable:true, resizable:true,width:100,cellEditor:'agSelectCellEditor',cellEditorParams:{cellHeight:40,values:['none','pending','nest-ready','nested','cut S','cut F','sand S','sand F','spray S','spray F','tfd S','tfd F','assy S','assyF','outsourced']}}, 
    {headerName: "Color", field: 'color', filter: true , editable:true, sortable:true, resizable:true,width:90,cellEditor:'agSelectCellEditor',cellEditorParams:{cellHeight:40,values:['w','chPr','aria','b-block','b-rum','g-rose','other']}},
    {headerName: "DrType", field: "drType", filter: true , editable:true, sortable:true, resizable:true,width:90,cellEditor:'agSelectCellEditor',cellEditorParams:{cellHeight:40,values:['Shaker','4K','FlatMel','FlatTFD','ShkrV','LadV','Pillow','other']}},
    {headerName: "OrdAC", field: "ordAC", filter: true , editable:true, sortable:true, resizable:true,width:90},
    {headerName: "OrdBy", field: "ordBy", filter: true , editable:true, sortable:true, resizable:true,width:82}, 
    {headerName: "OrdVia", field: "ordVia", filter: true , editable:true, sortable:true, resizable:true,width:90,cellEditor:'agSelectCellEditor',cellEditorParams:{cellHeight:40,values:['em','ip','ph','sm','fx','-','vi']}},
    {headerName: "Footage", field: "footage", filter: true , editable:true, sortable:true, resizable:true,width:90},
    {headerName: "Note", field: "note", filter: true , editable:true, sortable:true, resizable:true,width:200},
    {headerName: "P.O.", field: "po", filter: true , editable:true, sortable:true, resizable:true,width:90}, 
    {headerName: "Inv", field: "inv", filter: true , editable:true, sortable:true, resizable:true,width:75},
    {headerName: "Wall 1", field: "wall1", editable:true, sortable:true, resizable:true,width:90},
    {headerName: "Wall 2", field: "wall2", editable:true, sortable:true, resizable:true,width:90}, 
    {headerName: "ApprD", field: "apprDate", filter: 'agDateColumnFilter', editable:true, sortable:true, resizable:true,width:110, cellEditor: 'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}}, 
    {headerName: "ReadyDate", field: "rdyDate", filter: 'agDateColumnFilter', editable:true, sortable:true, resizable:true,width:110, cellEditor: 'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}}, 
    {headerName: "OutDate", field: "outDate", filter: 'agDateColumnFilter', editable:true, sortable:true, resizable:true,width:110, cellEditor: 'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}}, 
    {headerName: "TFDDate", field: "tfdDate", filter: 'agDateColumnFilter', editable:true, sortable:true, resizable:true,width:110, cellEditor: 'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}}, 
    {headerName: "EdgeDate", field: "edgeDate", filter: 'agDateColumnFilter', editable:true, sortable:true, resizable:true,width:110, cellEditor: 'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}}, 
    {headerName: "SandDate", field: "sandDate", filter: 'agDateColumnFilter', editable:true, sortable:true, resizable:true,width:110, cellEditor: 'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}}, 
    {headerName: "SprayDate", field: "sprDate", filter: 'agDateColumnFilter', editable:true, sortable:true, resizable:true,width:110, cellEditor: 'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}}, 
    {headerName: "MelCutD", field: "melDate", filter: 'agDateColumnFilter', editable:true, sortable:true, resizable:true,width:110, cellEditor: 'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}}, 
    {headerName: "MDFCutD", field: "mdfDate", filter: 'agDateColumnFilter', editable:true, sortable:true, resizable:true,width:110, cellEditor: 'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}},
    {headerName: "Dwg", field: "dwg", filter: true , editable:true, sortable:true, resizable:true,width:90},
    {headerName: "DrMat", field: "drMat", filter: true , editable:true, sortable:true, resizable:true,width:90,cellEditor:'agSelectCellEditor',cellEditorParams:{cellHeight:40,values:['mel','mdf','no','-','?']}},
    {headerName: "Urgency", field: "urg", filter: true , editable:true, sortable:true, resizable:true,width:90}
  ]

// day-based filter template: {$or:[{assyDate:this.workDay(-1)},{assyDate:this.workDay(0)},{assyDate:this.workDay(1)}]};
// status-based filter template: {$or:[{ordSt:'processed'},{ordSt:'tasked'},{ordSt:'production'}]};
  ordersSort = [{colId:"entryDate",sort:"desc"},{colId:"id",sort:"desc"}];
  ordersProps = {columnDefs:this.allColumns,tableName:"orders",title:"AFO",setSubmit:(data)=>this.submitNewOrd=(data),sort:this.ordersSort};


  planColumns = ["_id","assyDate","id","imp","ordSt","client", "tag", "ordType", "color", "drType", "cabCt", "drCt","melSt","mdfSt","entryDate","tfdDate","edgeDate","sprayDate","sandDate","melDate","mdfDate"];
  planRows = {$or:[{ordSt:'approved'},{ordSt:'tasked'},{ordSt:'production'},{ordSt:'ready'},{ordSt:'assyFwaiting'}]};
  planSort = [{colId:"assyDate",sort:"asc"},{colId:"imp",sort:"desc"},{colId:"id",sort:"asc"}];
  //construct columnDefs for each page, filtering down allColums to the requested selection, then sorting by the order in the selection:
  planProps = {
    columnDefs:this.allColumns.filter(x=>{return this.planColumns.includes(x.field)}).sort((a,b)=>(this.planColumns.indexOf(a.field) >this.planColumns.indexOf(b.field)?1:-1)),
    colFilter:this.planColumns,
    rowFilter:this.planRows,
    sort:this.planSort,
    tableName: "plan",
    title:"Plan"
  };

  assyColumns = ["_id","assyDate","ordSt","melSt","mdfSt","id", "ordType", "color", "drType","client", "tag", "cabCt", "drCt","imp"];
  assyRows = {$or:[{assyDate:this.workDay(0)}]}; //{assyDate:this.workDay(-1)},,{assyDate:this.workDay(1)}
  assySort = [{colId:"assyDate",sort:"asc"},{colId:"imp",sort:"desc"},{colId:"id",sort:"asc"}];
  
  assyProps = {
    columnDefs:this.allColumns.filter(x=>{return this.assyColumns.includes(x.field)}).sort((a,b)=>(this.assyColumns.indexOf(a.field) >this.assyColumns.indexOf(b.field)?1:-1)),
    colFilter:this.assyColumns,
    rowFilter:this.assyRows,
    sort:this.assySort,
    tableName: "assy",
    title : "Assembly"
  };

  tfdColumns = ["_id","tfdDate","mdfSt","id", "ordType", "color", "drType","client", "tag", "cabCt", "drCt","imp"];
  tfdRows = {$or:[{tfdDate:this.workDay(0)}]};
  tfdSort = [{colId:"tfdDate",sort:"asc"},{colId:"imp",sort:"desc"},{colId:"id",sort:"asc"}];
  
  tfdProps = {
    columnDefs:this.allColumns.filter(x=>{return this.tfdColumns.includes(x.field)}).sort((a,b)=>(this.tfdColumns.indexOf(a.field) >this.tfdColumns.indexOf(b.field)?1:-1)),
    colFilter:this.tfdColumns,
    rowFilter:this.tfdRows,
    sort:this.tfdSort,
    tableName: "tfd",
    title : "TFD"
  };

  sandColumns = ["_id","sandDate","mdfSt","id", "ordType", "color", "drType","client", "tag", "cabCt", "drCt","imp"];
  sandRows = {$or:[{sandDate:this.workDay(0)}]};
  sandSort = [{colId:"sandDate",sort:"asc"},{colId:"imp",sort:"desc"},{colId:"id",sort:"asc"}];
  
  sandProps = {
    columnDefs:this.allColumns.filter(x=>{return this.sandColumns.includes(x.field)}).sort((a,b)=>(this.sandColumns.indexOf(a.field) >this.sandColumns.indexOf(b.field)?1:-1)),
    colFilter:this.sandColumns,
    rowFilter:this.sandRows,
    sort:this.sandSort,
    tableName: "sand",
    title : "Sand"
  };

  edgeColumns = ["_id","edgeDate","melSt","id", "ordType", "color", "drType","client", "tag", "cabCt", "drCt","imp"];
  edgeRows = {$or:[{edgeDate:this.workDay(0)}]};
  edgeSort = [{colId:"edgeDate",sort:"asc"},{colId:"imp",sort:"desc"},{colId:"id",sort:"asc"}];
  
  edgeProps = {
    columnDefs:this.allColumns.filter(x=>{return this.edgeColumns.includes(x.field)}).sort((a,b)=>(this.edgeColumns.indexOf(a.field) >this.edgeColumns.indexOf(b.field)?1:-1)),
    colFilter:this.edgeColumns,
    rowFilter:this.edgeRows,
    sort:this.edgeSort,
    tableName: "edge",
    title : "Edge"
  };
  
ganttDays = this.workdays.slice(200,300);
gCols = [
  {headerName: "Assy", pinned: 'left', field: "assyDate", editable:true, sortable:true, resizable:true,width:25, cellEditor: 'datePicker',cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}}, 
  {headerName: "#", pinned: 'left', field: "imp", editable:true, sortable:true,resizable:true,width:32},
  {headerName: "id", pinned: 'left', field: "id", editable:true, sortable:true,resizable:true,width:70},
  ...this.ganttDays.map((x,i)=>{return {headerName:this.formatDateMoDay({value:x}),field:x,width:100,resizable:true}})
];
ganttColumns = this.gCols;
ganttRows = {$or:[
  { outDate: { $ne: "" } },
  { assyDate: { $ne: "" } },
  { tfdDate: { $ne: "" } },
  { edgeDate: { $ne: "" } },
  { sprayDate: { $ne: "" } },
  { sandDate: { $ne: "" } },
  { melDate: { $ne: "" } },
  { mdfDate: { $ne: "" } },
]};
ganttSort = [{colId:"assyDate",sort:"desc"},{colId:"imp",sort:"desc"},{colId:"id",sort:"desc"}];

ganttProps = {
  columnDefs:this.ganttColumns,//this.allColumns.filter(x=>{return this.ganttColumns.includes(x.field)}).sort((a,b)=>(this.ganttColumns.indexOf(a.field) >this.ganttColumns.indexOf(b.field)?1:-1)),
  rowFilter:this.ganttRows,
  sort:this.ganttSort,
  tableName: "gantt",
  title : "Gantt Chart"
};

  render() {
    console.log(this.workDay(0));
    return (
      <div className="App">
        <Router>
        <Switch>
          <Route  exact path="/plan"  component={()=> <div> <Header/> <Orders {...this.planProps}/></div>} />
          <Route  exact path="/"  component={()=><div> <Header/><NewOrder cols={this.allColumns} submit={(data)=>this.submitNewOrd(data)}/><Orders {...this.ordersProps}/></div>} />
          <Route  exact path="/prod"  component={()=> <div><Header/><div><Orders {...this.assyProps}/></div><div><Orders {...this.tfdProps}/></div><div><Orders {...this.sandProps}/></div><div><Orders {...this.edgeProps}/></div></div>} />
          <Route  exact path="/assy"  component={()=> <Orders {...this.assyProps}/>} />
          <Route  exact path="/tfd"  component={()=> <Orders {...this.tfdProps}/>} />
          <Route  exact path="/sand"  component={()=> <Orders {...this.sandProps}/>} />
          <Route  exact path="/edge"  component={()=> <Orders {...this.edgeProps}/>} />
          <Route  exact path="/gantt"  component={()=> <div><Header/><Orders {...this.ganttProps}/></div>} />
        </Switch>
        </Router>
      </div>
    );
  }
}

export default App;