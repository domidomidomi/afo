import React, { Component } from 'react';
import io from 'socket.io-client';
import './datepicker.css';
import './App.css';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-dark.css';
import $ from 'jquery';
window.jquery = window.jQuery = $;


let socket = io('http://localhost:3001/');

class Assy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columnDefs: [
        {headerName: "AssemblyDate", field: "assyDate", editable:true, resizable:true, cellEditor: 'datePicker',width:70,cellRenderer:data=>{return data.value?this.formatDateMoDay(data):''}},
        {headerName: "Order State", field: "ordSt", editable:true, resizable:true,width:90,cellEditor:'agSelectCellEditor',cellEditorParams:{cellHeight:40,values:['pending','processed','tasked','production','finished','out']}},
        {headerName: "id", field: "id", editable:true, resizable:true,width:70},
        {headerName: "Street", field: "street", editable:true, resizable:true}, 
        {headerName: "Order Type", field: "ordType", editable:true, resizable:true},
        {headerName: "Color", field: "color", editable:true, resizable:true},
        {headerName: "Door Type", field: "drType", editable:true, resizable:true},
        {headerName: "Client", field: "client", editable:true, resizable:true}, 
        {headerName: "Tag", field: "tag", editable:true, resizable:true}, 
        {headerName: "Cab Count", field: "cabCt", editable:true, resizable:true},
        {headerName: "Door Count", field: "drCt", editable:true, resizable:true},
        {headerName: "Note", field: "note", editable:true, resizable:true}
      ],
      gridOptions:{components: { datePicker: this.getDatePicker() }},
      rowData: []
    }
  }


  componentDidMount() {
    var state_current = this;
    socket.emit("initial_data");//("assembly");//
    socket.on("get_data", this.getData);
    socket.on("change_data", this.changeData);

    let script = document.createElement("script");
    script.src="//code.jquery.com/ui/1.11.4/jquery-ui.js"
    script.async = true;

    document.body.appendChild(script);

  }

  changeData = () => {socket.emit("initial_data")};

  componentWillUnmount() {
    socket.off("get_data");
    socket.off("change_data");
  }

  editCellStart=(params)=>{
    //console.log(params)
  }

  handleCellChange=(e)=>{
    console.log(e);
    let data = {_id:e.data._id,id:e.data.id,name:e.colDef.field,newValue:e.newValue,oldValue:e.oldValue}
    socket.emit("editVal",data);
  }

  getDatePicker=()=>{
    function Datepicker() {}
    Datepicker.prototype.init = function(params) {
      this.eInput = document.createElement("input");
      this.eInput.value = params.value;
      $(this.eInput).datepicker({ dateFormat: "m/d/yy" });
    };
    Datepicker.prototype.getGui = function() {
      return this.eInput;
    };
    Datepicker.prototype.afterGuiAttached = function() {
      this.eInput.focus();
      this.eInput.select();
    };
    Datepicker.prototype.getValue = function() {
      return this.eInput.value;
    };
    Datepicker.prototype.destroy = function() {};
    Datepicker.prototype.isPopup = function() {
      return false;
    };
    return Datepicker;
  }

  getData = (ords) => {
    this.setState({ rowData: ords },()=>{console.log(this.state.rowData)});
    return null;
  };

  formatDateMoDay=(data)=>{
    // data arrives as object with key 'value', which contains the date, formatted as 'm/dd/yyyy'
    let raw = data.value?data.value.split('/'):"";
    let mo={0:'',1:'Jan',2:'Feb',3:'Mar',4:'Apr',5:'May',6:'Jun',7:'Jul',8:'Aug',9:'Sep',10:'Oct',11:'Nov',12:'Jan'};//,01:'Jan',02:'Feb',03:'Mar',04:'Apr',05:'May',06:'Jun',07:'Jul',08:'Aug',09:'Sep'};
    return mo[parseInt(raw[0])]+' '+raw[1];
  }

  render() {
    return (
      <div 
        className="ag-theme-dark"
        style={{ 
        height: '500px', 
        width: '1100px' }} 
      >
        <AgGridReact
          gridOptions={this.state.gridOptions}
          onCellEditingStarted={params=>this.editCellStart(params)}
          onCellValueChanged={e=>this.handleCellChange(e)}
          columnDefs={this.state.columnDefs}
          rowData={this.state.rowData}>
        </AgGridReact>
      </div>
    );
  }
}

export default Assy;